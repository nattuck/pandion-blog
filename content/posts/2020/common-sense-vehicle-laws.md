---
title: "Common Sense Vehicle Laws"
date: 2020-08-13T01:10:43-04:00
draft: true
---

Over 100 people die every day in incidents involving dangerous vehicles.

This has been going on for far too long. 

We need common sense vehicle laws.

Almost all of these deaths involve vehicles with fully automatic transmissions.
It's impractical to get these dangerous death machines off the streets right
away, but we can make progress by banning the sale of new automatic vehicles. To
better keep track of these vehicles and to keep the existing supply out of the
hands of criminals, we will introduce a national registry. All existing vehicles
must be registered by one year after the introduction of the new policy, and the
registration or transfer of any vehicle will require the purchase of a $5000 tax
stamp.

In addtion to automatic vehicles, we need to control the proliferation of
military-style, high capacity assault vehicles. Nobody needs to drive to work in
a tank, so we will ban the sale of new assult vehicles entirely. An assault
vehicle is defined as a high capacity vehicle (capable of seating more than
three adults) with any one of the following military-style features: Grenade
Launcher, Trailer Hitch, Plow Mount, Four Wheel Drive, Adjustable Steering
Wheel, Tinted Windows. Further, in order to avoid aftermarket modifications that
could create a high capacity vehicle, any vehicle with more than three doors
into the passenger compartment will be considered high capacity reguardless of
the number of factory installed seats. 

In order to minimize the impact on collectors, these new rules will only apply
to produced in the year 1999 or later.

The sooner we can get these common sense vehicle laws passed, the sooner we can
get these death machines off our streets, and the sooner our children can be
safe once again.

Remember - nobody needs a high capacity assault vehicle for legitimate sporting
use.

