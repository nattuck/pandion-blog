---
title: "Moving to Hugo"
date: 2020-05-10T18:05:03-04:00
---

I've been relying on the [Jekyll](https://jekyllrb.com/) static site generator
as my primary way to maintain a web site for a while, but it's gotten a bit slow
on large sites so it's time to try something new. [Hugo](https://gohugo.io/) is
a very similar tool with some optimization for speed.

This is the second paragraph of this post.

Third.

Fourth.
