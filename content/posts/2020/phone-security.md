---
title: "Phone Security"
date: 2020-07-02T22:40:48-04:00
draft: true
---

Cell network:

 - Your phone talks to cell towers.
   - Your location is recorded: which tower?
   - Your location is recorded: triangulation.
   - Your location can be intercepted: anyone with a radio receiver.
 - Cellphone data is weakly encrypted.
   - This is phone calls and SMS / MMS.
   - 4g is encrypted OK.
   - Tower owner can access data; stingray means tower owner.

Internet:

 - Cell tower owner acts as a router.
 - Assume full access for unencrypted data.
 - Can do traffic analysis on encrypted data.

Universal Backdoor

 - If software has automatic updates, then those updates can be
   arbitrary software.
 - If the software also requires a user account, then it can deliver
   user-specific updates. These updates can do arbitrary stuff.
 
Apps
