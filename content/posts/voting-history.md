---
title: "Voting History"
date: 2020-05-23T13:53:03-04:00
draft: true
---





References:

Voting History / Methods

 * [CBS: America's History of
   Voting](https://www.youtube.com/watch?v=LWECwclJg8M)
 * [MIT Election Lab: Vote by
   Mail](https://electionlab.mit.edu/research/voting-mail-and-absentee-voting)
 * [Voting Machine
   Timeline](https://votingmachines.procon.org/historical-timeline/)
 * [History of Voting](https://homepage.divms.uiowa.edu/~jones/voting/pictures/)
 * [How Amercians Have
   Voted](https://www.history.com/news/voting-elections-ballots-electronic)
 * [Britannica: Timeline on sufferage
   expansion](https://www.britannica.com/story/voting-in-the-usa)
 * [ACLU: Voting Rights Act Dates](https://www.britannica.com/story/voting-in-the-usa) 

Electoral Fraud

 * Wikipedia: https://en.wikipedia.org/wiki/Electoral_fraud


Suspicion of Rigged Elections

 * https://time.com/4536566/rigged-election-american-history/ (1960 Nixon vs JFK)

Cases of Election Fraud:

 * https://lawandcrime.com/high-profile/michigan-election-official-charged-with-six-counts-of-felony-election-fraud/
 * https://www.washingtonexaminer.com/opinion/npr-report-ignores-brenda-snipes-history-of-election-tampering
 
Two Party System:

 * Election of 1824: Stalemate https://en.wikipedia.org/wiki/1824_United_States_presidential_election


