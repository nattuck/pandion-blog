---
title: "Debian Router"
date: 2023-10-10T9:35:54-04:00
---

## Enable Forwarding

Enable IPv4 forwarding: /etc/sysctl.conf

For ipv6 to work right, need to enable stateless
autoconf on public interface (enp2s0 below).

```
net.ipv4.ip_forward = 1
net.ipv6.conf.all.forwarding = 1
net.ipv6.conf.enp2s0.accept_ra = 2
```

## Enable NAT

Here's a NAT config with no filtering, it just routes from the private
network to the public network and back with masquerading as needed.

```
#!/usr/sbin/nft -f

flush ruleset

define DEV_PRIVATE = enp3s0
define DEV_WORLD = enp2s0
define NET_PRIVATE = 192.168.90.0/24

table inet default {
	chain input {
		type filter hook input priority filter;
	}
	chain forward {
		type filter hook forward priority filter;
	}
	chain output {
		type filter hook output priority filter;
	}
	chain prerouting {
		type nat hook prerouting priority 0; policy accept;
	}
	chain postrouting {
        type nat hook postrouting priority 100; policy accept;
		ip saddr $NET_PRIVATE oifname $DEV_WORLD masquerade
	}
}
```


## Enable DHCP

```bash
sudo apt install kea
```

Edit the config file in /etc/kea/kea-dhcp4.conf, making sure:

 - Only listen on correct interface
 - 


References:

 - [Kea DHCP Server](https://www.isc.org/kea/)
 - [NFTables Router](
 https://wiki.nftables.org/wiki-nftables/index.php/Simple_ruleset_for_a_home_router)
 - [NFTables NAT](
 https://wiki.nftables.org/wiki-nftables/index.php/Performing_Network_Address_Translation_(NAT))
