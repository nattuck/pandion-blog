---
title: "Hugo Setup"
date: 2021-05-27T21:19:41-04:00
draft: true
---

## Static Site Generator? 


## Why Hugo?


## Setup

Mostly following the instructions from the 
[Hugo Quick Start Guide](https://gohugo.io/getting-started/quick-start/).

### Install Hugo

  - On Debian: ```sudo apt install hugo```
  - Fallback: It's a Go program, so you can just download a release
    and run it.

### Create New Site

Working from a terminal window in the directory where we want to put
our project.

We use "hugo new site" to create a new site.

```
$ hugo new site critters
```

Next, we want to initialize a git repository in our newly created
site directory.

```
$ cd critters
$ git init .
```

Then, we want to select a theme to start with.

 - Theme list: https://themes.gohugo.io/
 - Selected theme: 
   [Hugo Bootstrap v4 Blog](https://themes.gohugo.io/hugo-theme-bootstrap4-blog/)

The quick start guide shows using a git submodule for the theme. This makes it
easier to get updates for that theme if the author changes it, but at the cost
of some added complexity. Instead, we're just going to add a copy of the theme 
to our site and store that copy in our git repository.

One major benefit to making our own copy of the theme is that we can then easily
make site-specific changes to the theme if we want.

In the themes directory, make a copy of the theme by cloning it and then
removing the git metadata so it's no longer its own git repository.

```
$ cd themes
$ git clone https://github.com/alanorth/hugo-theme-bootstrap4-blog.git
$ cd hugo-theme-bootstrap4-blog
$ rm -rf .git
```

Hugo themes generally provide an example config file. We want to copy that
over our site config file and then edit it.

(from the theme directory)

```
$ cp exampleSite/config.toml ../../config.toml 
```

That's sufficient to take a look at our site:

(from the root site directory)

```
$ hugo serve
```

As the output says, we can view our site at http://localhost:1313/

We see:

 * The layout for the theme.
 * A bunch of defaults.

We can change those defaults by editing that config file:

Now we'll up config.toml and change the following lines:

```
baseurl = "https://critters.fogcloud.org/"
...

```
