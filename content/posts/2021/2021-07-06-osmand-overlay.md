---
title: "Osmand Overlay"
date: 2021-07-06T23:35:54-04:00
---

Setting up a raster image as an overlay in osmand is kind of tricky.

Two resources:

 - https://shallowsky.com/blog/mapping/osmand-making-overlay-maps.html
 - https://docs.osmand.net/en/main@latest/osmand/map/raster-maps
