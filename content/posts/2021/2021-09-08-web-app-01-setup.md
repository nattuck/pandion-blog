---
title: "2021 09 08 Web App 01 Setup"
date: 2021-09-08T12:32:33-04:00
draft: true
---

# Get an Editor

A good compromise to start with is [Atom](https://atom.io/).

# Get Node.js

First, install [Node Version Manager](https://github.com/nvm-sh/nvm)

Then install the latest node LTS:

```
nvm install --lts
```

# Platform Overview

A web application is a computer program with parts that run in two separate
places:

 - A Web Server
 - A Web Browser

The part of the application that the user interacts with is a web site displayed
in the web browser. We construct the website using:

 - HTML, CSS for layout, content, and styling.
 - JavaScript for dynamic behavior.

The server supports the website in a couple of ways:

 - Web app gets downloaded to browser from server.
 - Server stores data outside the browser.
   - This allows a user to keep their data even if they clear their browser
     storage.
   - This can allow a user to visit the site from multiple browsers/devices and
     keep their data.
   - This can allow data to be shared between multiple users.

# Example 1: Counter

Counter app:

```
$ npx create-next-app counter
```

```
function Counter() {
  const [count, setCount] = useState(0);
 
  function plusOne(ev) {
    ev.preventDefault();
    setCount(count + 1);
  }
 
  return (
    <p>Counter: { count }</p>
    <p><button onClick={plusOne}>+1</button></p>
  );
}
```

See the next docs for how to run the dev server:

https://nextjs.org/learn/basics/create-nextjs-app/setup
