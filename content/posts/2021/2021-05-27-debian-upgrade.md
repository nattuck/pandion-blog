---
title: "Upgrading to Debian 11 Quick"
date: 2021-05-27T09:00:00-04:00
---

The [Debian Wiki](https://wiki.debian.org/DebianUpgrade) provides some
instructions to upgrade to a new release, but for systems with a lot of packages
installed the process can be really slow.

Here's the process I use to upgrade to a new Debian release.

<!--more-->

## Step 1: Disable Non-Debian Repositories

**sources in sources.list.d**

Look for third-party repos and disable them:

```
$ sudo su -
# cd /etc/apt
# ls sources.list.d
source1.list
source2.list
...
```

For each one of those apt sources, we want to open it up with a text editor
and comment out the source lines.

```
# ls sources.list.d
syncthing.list
# nano sources.list.d/syncthing.list
```

We want to comment out every line that specifies a "deb" source, so we go from
this:

```
deb https://apt.syncthing.net/ syncthing stable
```

To this:

```
# deb https://apt.syncthing.net/ syncthing stable
```

Do this for for every file in sources.list.d

**sources in sources.list**

Once we've cleaned up that directory, next is the main sources.list file.

```
# pwd
/etc/apt
# nano sources.list
```

In this file, there are two kinds of sources: official Debian sources and more
third-party sources.

```
# Official sources frequently are at debian.org, like this one:
deb http://ftp.us.debian.org/debian/ buster main

# Sometimes they're from official mirror servers like this one:
deb http://debian.csail.mit.edu/debian/ buster main

# This is definitely a third party repository:
deb http://emacs.ganneff.de/ buster main

# And this isn't even a real debian repo, it's an Ubuntu PPA
#  - it says "ppa"
#  - it's at "launchpad.net"
#  - it has an Ubuntu codename ("impish"), not a Debian one (like "buster")
deb http://ppa.launchpad.net/kdenlive/kdenlive-stable/ubuntu impish main
```

In sources.list.d, we want to comment out:

 - Things that aren't official Debian repos.
 - All the lines that don't start with "deb" (e.g. "deb-src"). They just
   slow things down for the update.

## Step 2: Install current upgrades

```
$ sudo apt update -y
$ sudo apt upgrade  -y
$ sudo apt autoremove -y
```

## Step 3: Switch your sources to the new Debian version

Back in sources.list.d, replace all instances of the old Debian version with the
new one. To go to from Debian 10 to 11, this means replacing "buster" with
"bullseye".

```
$ sudo nano /etc/apt/sources.list.d
```

```
# Edit lines like this:
deb http://ftp.us.debian.org/debian/ buster main
deb http://debian.csail.mit.edu/debian/ buster main

# To be lines like this:
deb http://ftp.us.debian.org/debian/ bullseye main
deb http://debian.csail.mit.edu/debian/ bullseye main
```


## Step 4: Do the upgrade

First, let's install a tool to speed up the process:

```
$ sudo apt install eatmydata
```

The eatmydata command runs another command, but skips all the steps where that
command tries to force data to really be written to disk. The Debian dpkg tool
forces disk rights frequently to avoid creating an unbootable system if the
machine crashes, but this is *really* slow with hundreds of packages.

Losing power in the middle of the next step would be bad - it might leave the
computer unbootable - but that problem can be easily fixed by booting from a
thumb drive and this doesn't risk user data at all.

**really do the upgrade**

This step will take like an hour, and will occasionally stop to ask
dumb questions.

```
$ sudo eatmydata apt update -y 
$ sudo eatmydata apt full-upgrade -y
$ sudo eatmydata apt autoremove -y
```

Handling dumb questions:

 - Do you want to restart services? Yes.
 - Do you want to install the package maintainer's version of the config
   file? Yes, unless you recognize it.

## Step 5: Reboot

```
$ sudo reboot
```

When the machine comes back up, you should be done.






