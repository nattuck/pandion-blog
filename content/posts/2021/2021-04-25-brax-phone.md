---
title: "Braxmobile Phone Unboxing"
date: 2021-04-25T18:00:00-04:00
---

Smartphones are a nightmare for privacy and personal data security.

One of the best options for a safe phone is an Android phone with a custom ROM
that doesn't include the standard apps and libraries from Google.

Finding a phone that supports custom ROMs can take some effort, and then getting
a ROM installed takes more effort. It'd be nice to just buy a phone already
set up with Google-free Android, but there aren't that many good options out
there.

[Rob Braxman](https://brax.me/home/rob) - the "internet privacy guy" sells
pre-installed "deGoogled" Android phones. Let's see if his offering is a good
deal.

## Selection and Pricing

Rob offers several models:

 - Google Pixel 3 & 3 XL for ~$400
 - Google Pixel 4 & 4 XL for ~$650
 - Moto G7 for ~$330
 - OnePlus 6 for ~$400

He also offers an installation service on a customer-provided phone for $130.

Looking at eBay for used phone prices, it looks like he's charging a bit over
market. But there's an excellent reason for this: Ordering used phones doesn't
always succeed. 

In addition to the obvious problem of getting defective phones, there's a
widespread problem of mislabeling phones. For many of the phones he provides
there's an unlocked version that supports custom ROM installs and a locked
(usually Verizon) edition with a locked bootloader that prevents custom software
installs. The unlocked version is worth more, so frequently the locked version
is mislabeled.

Overall the selection and pricing is reasonable. My only complaint is the lack
of the Google Pixel 3a - that model has a headphone jack.

## Shipping

I ordered a Google Pixel 3 on Wednesday, April 21st.

The phone shipped 2-day US priority mail from California to Massachusetts and
arrived on Saturday the 24th. That's pretty quick.

## Unboxing

Packing was fine - bubble wrap around phone. Loose wall wart and cable. No
documentation included - would have liked to see an indication 

## OS Version

Settings -> About Phone Shows:

 * Device Name: BraxOS
 * Build: Lineage something

## Installed Apps

Stock Android:

 * Phone
 * Messaging
 * Camera

Reasonable Defaults:

 * F-Droid
 * Duck Duck Go
 * Conversations
 * K-9 Mail
 * OpenKeychain
 * OsmAnd
 * Orbot 
 * OpenVPN
 * Netguard

Other stuff:

 * MicroG
 * Brax.me
 * Waze

 * MicroG - Registers phone with Google. This is a risk to both privacy
   and autonomy.
 * Brax.Me - Rob Braxman's social network. Sure.
 * Waze - Unlikely to be more secure than Google Maps. It's owned by Google,
   after all.

## Summary Comments

 - Phone arrived in mint condition.
 - Lineage OS and a reasonable set of software came preinstalled.
 - MicroG is a privacy risk that should be carefully considered.
 - One issue: It looks like there's no automatic updates for software installed
   via F-Droid. I'll need to fix that to make this usable.
 - Conclusion: Overall, Rob Braxman is providing the service of preinstalling a
   custom ROM without Google apps reasonably well at a decent price.

## Follow-Up

**Verizon Activation**

This worked fine. Needed to contact Verizon to activate, but that sounds like a
Verizon problem.

**F-Droid Auto-installs**

Needed to install Magisk. This was more complicated than expected, but did
eventually work.

Steps:

 - Install Magisk APK
 - Unpack boot.img from Lineage OS zip (requires unpacking bin; don't use
   some other boot.img)
 - Copy to phone, patch with Magisk app, copy back, fastboot flash boot

**Aurora Store**

This seems to work at the moment.
