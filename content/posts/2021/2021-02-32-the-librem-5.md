---
title: "The Librem 5"
date: 2021-02-23T19:21:03-04:00
---

I ordered a reasonably early [Librem 5](https://puri.sm/products/librem-5/). Now
that I have it, I'd like to get it set up as my primary phone.

It's effectively an early beta product, so there's a bunch of issues. In this
post I'll track those issues so I can complain about them later.

<!--more-->

# PureOS

The OS on this thing is PureOS, a custom Debian variant by Purism.

It *should* basically be a standard Desktop Linux system with a new graphical
shell. It almost is.

## Root Account Intentionally Broken

```
purism@pureos:~$ sudo su -
Your account has expired; please contact your system administrator
su: Authentication failure
```

WTF?

This can be fixed easily enough, but the mindset that leads to this sort of
nonsense is the same shit that kills Ubuntu Touch. Your distro isn't
special, and your excuse for actually breaking root isn't valid.

```
# This fixes it.
sudo chage -E -1 root
```

## Default Browser is Broken

https://forums.puri.sm/t/librem-5-web-browser-flaw/11392/26

Update (2021/04): This was patched in a later release.

## User Account and Lockscreen

 - Forced numeric pin
 - Can't disable lockscreen

## Wifi Card Firmware Corruption

Update (2021/04):

Somehow the firmware on the WiFi card got corrupted, such that the Librem 5
could no longer connect to WiFi Networks.

 - Good news: Purism shipped me a WiFi card.
 - Bad news: Installing WiFi antenna connectors is hard.
 - Good news: I was able to ship back the phone to get it fixed.
 - Bad news: I don't have the phone back yet.

