---
title: "Autotools Video Tutorial"
date: 2013-05-19T17:59:00-04:00
aliases:
    - /2013/05/19/autotools-video-tutorial.html
---

I finally found a reasonable introduction to GNU autotools, in the form of a 
[screencast on Youtube](http://www.youtube.com/watch?v=4q_inV9M_us). This is
the standard way C/C++ code gets built, and it's got to be less evil than
CMake...
