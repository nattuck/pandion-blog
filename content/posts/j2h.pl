#!/usr/bin/perl
use 5.20.0;
use warnings FATAL => 'all';

use IO::Handle;

sub convert {
    my ($xx) = @_;
    say "converting [$xx]";
    my ($dd, $tt) = split(/\s+/, $xx, 2);
    return "${dd}T${tt}:00-04:00";
}

my $name = shift;

open(my $p1, "<", "$name");
open(my $p2, ">", "$name.conv");

say "$name => $name.conv";

my $marks = 0;
my $date = 0;
while (<$p1>) {
    chomp;
    if (/^---\s*$/) {
        if ($marks == 1) {

        }

        $p2->say($_);
        $marks++;
        next;
    }

    if ($marks < 2) {
        /^\S+:/ or next;
        my ($kk, $vv) = split /:\s*/, $_, 2;
        if ($kk eq "title") {
            $p2->say($_);
        }
        if ($kk eq "date") {
            say "d = $_";
            if ($vv =~ /\S\s+\S/) {
                my $d1 = convert($vv);
                say "converting date: $vv";
                $p2->say("date: $d1");
            }
            else {
                $p2->say($_);
            }
        }
    }
    else {
        $p2->say($_);
    }
}

close($p2);
close($p1);

rename("$name.conv", "$name");
