---
title: "Some Art, 2022-06-28"
type: page
date: 2022-06-28T00:14:33-04:00
draft: false
url: /ohy7xeiW
---

## Some Art

### Persephone
[![thumb-01-persephone.jpg](thumb-01-persephone.jpg)](01-persephone.jpg)

### Venus Anadyomene
[![thumb-02-venus_anadyomene.jpg](thumb-02-venus_anadyomene.jpg)](02-venus_anadyomene.jpg)

### Perseus and Andromeda
[![thumb-03-perseus-and-andromeda.jpg](thumb-03-perseus-and-andromeda.jpg)](03-perseus-and-andromeda.jpg)

### David
[![thumb-04-david.jpg](thumb-04-david.jpg)](04-david.jpg)

### Adjutant Stork
[![thumb-05-adjutant-stork.jpg](thumb-05-adjutant-stork.jpg)](05-adjutant-stork.jpg)

### Portrait of Pauline Manship
[![thumb-06-portrait-of-pauline-manship.jpg](thumb-06-portrait-of-pauline-manship.jpg)](06-portrait-of-pauline-manship.jpg)

### Portraits of Isabel Natti
[![thumb-07-portraits-of-isabel-natti.jpg](thumb-07-portraits-of-isabel-natti.jpg)](07-portraits-of-isabel-natti.jpg)
