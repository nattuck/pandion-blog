#!/bin/bash

convert 01-persephone.jpg -resize 960x960 thumb-01-persephone.jpg
convert 02-venus_anadyomene.jpg -resize 960x960 thumb-02-venus_anadyomene.jpg
convert 03-perseus-and-andromeda.jpg -resize 960x960 thumb-03-perseus-and-andromeda.jpg
convert 04-david.jpg -resize 960x960 thumb-04-david.jpg
convert 05-adjutant-stork.jpg -resize 960x960 thumb-05-adjutant-stork.jpg
convert 06-portrait-of-pauline-manship.jpg -resize 960x960 thumb-06-portrait-of-pauline-manship.jpg
convert 07-portraits-of-isabel-natti.jpg -resize 960x960 thumb-07-portraits-of-isabel-natti.jpg
