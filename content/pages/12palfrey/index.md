---
title: "12 Palfrey Notes, Aug 2021"
type: page
date: 2021-06-01T00:14:33-04:00
draft: false
url: /12palfrey
---

# 12 Palfrey St Notes, 2021 Aug 29

## Utilities

 - Electric is Eversource: https://www.eversource.com
 - Natural Gas is National Grid: https://online.nationalgridus.com

## Solar System

As part of the lease agreement for the solar panels on the roof, the monitoring
system needs to stay connected to the internet. Specifically, the SolarCity box
needs to stay plugged into an internet-connected ethernet port.

In the picture below there are two cables plugged into the SolarCity box (the
one that says "SolarCity" on it). The black cable is power, which it needs. The
white cable is Ethernet, which wants to be connected to an ethernet port with
internet connectivity.

Currently, it's hooked up to the switch (black box in the picture) which is
hooked to a router, which is hooked up to the Verizon FiOS fiber optic line. If
the router is swapped out, the new one can go in the same place or - once the
current router is unplugged - an ethernet cable could be run from a new router
to any of the ethernet ports in the house.

![02-solarcity.jpg](./02-solarcity.jpg)

When opening the Electric account with Eversource, tenant will need to
explicitly tell an Eversource rep that the house has solar and that they want
net metering, and then go through whatever the steps are to get that set up.

## Network

The house is wired for hardwired ethernet. This involves a network switch in the
basement and ethernet ports in most of the rooms. Since Verizon FIOS comes into
the basement, there's a wireless access point in the living room to provide
strong wireless signal to the house.

![01-net-switch.jpg](./01-net-switch.jpg)
![05-wireless.jpg](./05-wireless.jpg)

The Wifi password for the access point is on the card next to it.

## Master Bedroom Light Switch

The switch for the overhead light and fan in the master bedroom is wireless.

![12-light-switch.jpg](./12-light-switch.jpg)

## Stuff left in the house

The following movable objects are in the house:

 * TV + Sound Stuff
 * Outdoor Deck Table + Parasol + Bench + Chairs 
 * Sturdy Shelving in the Basement
 * Electric car charger
 * Washer and Dryer
 
These go with the house and shouldn't get thrown out.
 
![04-tv.jpg](./04-tv.jpg)
![07-deck-table.jpg](./07-deck-table.jpg)
![15-basement-shelf.jpg](./15-basement-shelf.jpg)
![17-good-shelf.jpg](./17-good-shelf.jpg)
![08-car-charger.jpg](./08-car-charger.jpg)
![03-washer-dryer.jpg](./03-washer-dryer.jpg)

