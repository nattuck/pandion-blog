#!/bin/bash

convert ~/Pictures/12palfrey/01-net-switch.jpg -resize 960x960 01-net-switch.jpg
convert ~/Pictures/12palfrey/02-solarcity.jpg -resize 960x960 02-solarcity.jpg
convert ~/Pictures/12palfrey/03-washer-dryer.jpg -resize 960x960 03-washer-dryer.jpg
convert ~/Pictures/12palfrey/04-tv.jpg -resize 960x960 04-tv.jpg
convert ~/Pictures/12palfrey/05-wireless.jpg -resize 960x960 05-wireless.jpg
convert ~/Pictures/12palfrey/06-dining-table.jpg -resize 960x960 06-dining-table.jpg
convert ~/Pictures/12palfrey/07-deck-table.jpg -resize 960x960 07-deck-table.jpg
convert ~/Pictures/12palfrey/08-car-charger.jpg -resize 960x960 08-car-charger.jpg
convert ~/Pictures/12palfrey/09-crib.jpg -resize 960x960 09-crib.jpg
convert ~/Pictures/12palfrey/10-bunkbeds.jpg -resize 960x960 10-bunkbeds.jpg
convert ~/Pictures/12palfrey/11-dressers.jpg -resize 960x960 11-dressers.jpg
convert ~/Pictures/12palfrey/12-light-switch.jpg -resize 960x960 12-light-switch.jpg
convert ~/Pictures/12palfrey/13-main-bed.jpg -resize 960x960 13-main-bed.jpg
convert ~/Pictures/12palfrey/14-main-dressers.jpg -resize 960x960 14-main-dressers.jpg
convert ~/Pictures/12palfrey/15-basement-shelf.jpg -resize 960x960 15-basement-shelf.jpg
convert ~/Pictures/12palfrey/16-shelf-2.jpg -resize 960x960 16-shelf-2.jpg
convert ~/Pictures/12palfrey/17-good-shelf.jpg -resize 960x960 17-good-shelf.jpg
convert ~/Pictures/12palfrey/18-fridge.jpg -resize 960x960 18-fridge.jpg
convert ~/Pictures/12palfrey/19-vaccuum.jpg -resize 960x960 19-vaccuum.jpg
