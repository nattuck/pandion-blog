
USER := nat
HOST := coyote.ferrus.net
RELP := ~/www/pandion.ferrus.net
BASE := https://pandion.ferrus.net/

deploy:
	rm -rf public
	hugo --baseURL "$(BASE)"
	rsync -avz --delete public/ "$(USER)@$(HOST):$(RELP)"

ship: deploy

s: ship

clean:
	rm -rf public

.PHONY: deploy
